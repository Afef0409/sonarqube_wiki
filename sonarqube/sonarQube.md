# Get started with SonarQube 9.1

  ![logo SonarQube](images/sonarqube-logo.png)

##  C'est quoi SonarQube ?

SonarQube est un logiciel open-source développé par la société SonarSource. Il permet de mesurer la qualité de votre projet de plusieurs façons.

## Les avantages de SonarQube:

* Open source (Source code on GitHub)
* Multi language support (Java, C#, Python, JavaScript, C++, TypeScript, XML, PL/SQL , et d'autres)
* IDE integration - __SonarLint__ for Visual Studio, Eclipse etc.
* Build tool integration - MSBuild, Ant, Maven, Gradle, TFS etc.
* Extensibility with plugins & custom rules
* Authentication & authorization choices - LDAP, Azure, Google, GitHub, GitLab and more.
* Source control - SVN & Git supported out of the box, more with plugins

### Analyse de code continu avec SonarQube et analyse en codant grace è __SonarLint__:

![sonarqube failed result](images/sonarqube-cycle.png)

## QU'EST-CE QU'ON PEUT FAIRE AVEC ?

Respect des règles et normes du code  
Documentation du code  
Analyse des tests unitaires (couverture du code, etc.)  
Duplication du code  
Vulnérabilités potentielles (par degré d’importance : mineure, majeure, bloquante)  
Génération de rapports  
Compatibilité avec GitLab et GitLab CI (pour de l’intégration continue)  

## ET COMMENT ÇA FONCTIONNE ?

SonarQube comprend en fait “deux sous-applications” :
* un moteur d’analyse (le scanner):  
    * installé localement sur la machine du développeur.  

*  un serveur centralisé :  
    * pour la conservation des enregistrements et la création de rapports.  
    ### SonarQube structure:  

    ![sonarqube failed result](images/sonarqube_structure.png)

 #### Remarque:  

 __Une seule instance de serveur SonarQube__ est capable de prendre en charge __plusieurs scanners__, ce qui vous permet de centraliser les rapports sur la qualité du code provenant de nombreux développeurs en un seul endroit.

 D’autre part, SonarQube sait s’adapter à la version de votre langage et est ainsi capable de vous signaler du code déprécié  

### SonarQube : test reussi

![sonarqueb passed result](images/sonarqube-result-exp.png)  

### SonarQube : test echoué

![sonarqube failed result](images/result-failed.png)  

## POURQUOI IL EST PRIMORDIAL D'AVOIR UN CODE DE QUALITÉ ?  

Un code de qualité facilitera la maintenance de votre projet.  
Votre code sera plus modulaire et beaucoup plus exploitable si d’autres développeurs devaient se charger de reprendre votre flambeau.  
En plus, vous réduisez le risque de “bug” et vous augmentez les performances de votre solution (plus de duplication de code, plus de code inutile, plus de code déprécié, etc.).  
Bref, votre code sera “tout propre et en bonne santé”.  

 __Prenez soin de votre code, utilisez SonarQube.__  

 ## Installation  de SonarQube:

 La méthode la plus simple est d'installer SonarQube via une image Docker.

 ## ÉTAPE 1 : INSTALLEZ DOCKER

 Pour MacOs : https://www.docker.com/products/docker-desktop Pour Windows : https://store.docker.com/editions/community/docker-ce-desktop-windows Pour Ubuntu & Debian, tapez cette commande dans votre terminal :

>  curl https://releases.rancher.com/install-docker/18.03  


## ÉTAPE 2 : INSTALLEZ SONARQUBE COMMUNITY ET LANCEZ-LE:

Dans votre terminal, tapez les 2 commandes suivantes :

> docker pull sonarqube  

> docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube

Vous pouvez maintenant accéder au serveur SonarQube et consulter l’interface sur    http://localhost:9000 . (ça peut durer quelques secondes avant de pouvoir accéder à l'interface...)  

![sonarqube failed result](images/first-cnx.png)  

Vous pouvez vous connecter en utilisant le couple admin/admin.

## ÉTAPE 3 : CONFIGUREZ L'ANALYSE DE VOTRE PROJET

A la racine de votre projet, créez un fichier sonar-project.properties et copiez-y le contenu suivant :

    #Doit être unique dans l'instance SonarQube
    sonar.projectKey=votre_clé_unique

    #Le nom et la version affichés dans l'interface SonarQube
    sonar.projectName=MonProjet
    sonar.projectVersion=1.0

    #Le chemin relatif aux fichiers que vous vous souhaitez analyser dans votre code

    sonar.sources=src

    #Cas d'un projet utilisant composer. 
    #On ignore l'analyse du code présent dans les dépendances.

    sonar.exclusions=vendor/**

    #Encodage du code source.
    sonar.sourceEncoding=UTF-8

## Étape 4 : lancez le scanner  Dans votre terminal, dirigez-vous à la racine de votre projet et tapez la commande suivante :

> docker run -ti -v votre_chemin_projet/:/root/src --link sonarqube newtmitch/sonar-scanner

## ÉTAPE 5 : CONSULTEZ LE RAPPORT GÉNÉRÉ

Lorsque vous voyez “ANALYSE RÉUSSIE”, vous pouvez retourner sur l’interface de SonarQube (http://localhost:9000). Vous pouvez constater que votre projet a bien été analysé.  

![sonarqube failed result](images/test-result.png) 